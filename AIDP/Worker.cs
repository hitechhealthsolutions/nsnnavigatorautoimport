using ExcelDataReader;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Net.Mail;

namespace AIDP
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        public string? basePath;
        public string zipFile;
        public string extractPath;
        public string backupFolder;
        public string fileNumber;
        public string importFile;
        Connection cs = new Connection();
        Helper helper = new Helper();

        public Worker(ILogger<Worker> logger)
        {
            _logger = logger;
            //this.basePath = null;
            this.zipFile = String.Empty;
            this.extractPath = String.Empty;
            this.backupFolder = String.Empty;
            this.fileNumber = String.Empty;
            this.importFile = String.Empty;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Worker started at: {time}", DateTimeOffset.Now);
            DownloadFile();
        }
        private void DownloadFile()
        {
            _logger.LogInformation("Worker before set folder path");
            this.setFolderPaths();
            this.emptyExtractFolder();

            string zipFile = this.basePath+this.zipFile;
            string extractPath = this.extractPath;            
            System.IO.Compression.ZipFile.ExtractToDirectory(zipFile, extractPath);
            _logger.LogInformation("Worker after extracting");
            this.importFile = "NSNR_ns_"+this.fileNumber+".csv";
            if(this.zipFile == "")
            {
                _logger.LogInformation("No zip found.");
                return;
            }
            this.UploadFile();
            this.removeFilesAndFolders();
        }

        private void removeFilesAndFolders()
        {
            try
            {
                this.emptyExtractFolder();
                
                //moving zip folder to "backup" folder.
                Directory.Move(this.basePath + this.zipFile, this.backupFolder + this.zipFile);
            }
            catch (Exception e)
            {
                _logger.LogInformation("Fails to move/delete files."+ e.Message); 
            }
        }
        private void emptyExtractFolder()
        {
            DirectoryInfo baseDir = new DirectoryInfo(this.extractPath);
            FileInfo[] Files = baseDir.GetFiles(); //Getting all files
            foreach (FileInfo File in Files)
            {
                //Deleting all files from extract folder.
                System.IO.File.Delete(File.FullName);
            }
        }

        private void setFolderPaths()
        {            
            this.getZipFile();
            this.backupFolder = this.basePath + "backup\\";
            this.extractPath = this.basePath + "extract";            
        }
        private void getZipFile()
        {
            DataTable configPathDt = helper.getConfigPath();
            _logger.LogInformation("config patsh." + configPathDt.Rows.Count);
            if(configPathDt.Rows.Count < 1)
            {
                _logger.LogInformation("No rows found in db." + configPathDt.Rows.Count);
                this.basePath = @"E:\xampp\htdocs\NsnNavigator\NSNNavigatorAPI\NSNNaviagtorAPI\ClientImport\daily_import\";
            } else
            {
                this.basePath = configPathDt.Rows[0]["ConfigValue"].ToString(); //E:\xampp\htdocs\NsnNavigator\NSNNavigatorAPI\NSNNaviagtorAPI\ClientImport\daily_import\
            }
            
            DirectoryInfo baseDir = new DirectoryInfo(this.basePath);
            FileInfo[] Files = baseDir.GetFiles("*.zip"); //Getting zip files
            if(Files.Count() < 1)
            {
                _logger.LogInformation("No zip found.");
                return;
            }

            foreach (FileInfo fi in Files)
            {
                this.zipFile = fi.Name;     //setting up zipfile
            }
            string[] fileName = Path.GetFileNameWithoutExtension(this.zipFile).Split('_');
            this.fileNumber = fileName[1];  //setting up fileNumber
        }
        public void UploadFile()
        {
            string FileKey = this.importFile + "-" + DateTime.Today.ToString("yyyyMMdd");
            int rowcont = 0;
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[16] { new DataColumn("PatientName", typeof(string)), new DataColumn("MRN", typeof(int)), new DataColumn("DOS", typeof(DateTime)), new DataColumn("PhysicianName", typeof(string)),
                new DataColumn("CaseID", typeof(int)), new DataColumn("CaseStatus", typeof(string)), new DataColumn("FacilityName", typeof(string)), new DataColumn("CPTCode", typeof(string)), new DataColumn("CPTDesc", typeof(string)), new DataColumn("ScheduleOrder", typeof(int)),
                new DataColumn("PayorName", typeof(string)), new DataColumn("GroupNumber", typeof(string)), new DataColumn("InsuranceID", typeof(string)), new DataColumn("FileKey", typeof(string)),
                new DataColumn("UploadDateTime", typeof(DateTime)), new DataColumn("Username", typeof(string)) });
            string filename = this.extractPath +"/"+ this.importFile;
            string filetype = Path.GetExtension(filename).ToLower();
            try
            {
                System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
                using (var stream = System.IO.File.Open(filename, FileMode.Open, FileAccess.Read))
                {
                    IExcelDataReader reader;
                    if (filetype == ".csv")
                        reader = ExcelReaderFactory.CreateCsvReader(stream);
                    else
                        reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                    _logger.LogInformation("Worker before creating dt");
                    while (reader.Read())
                    {
                        rowcont = rowcont + 1;
                        DataRow dr = dt.NewRow();
                        dr["PatientName"] = reader.GetValue(0).ToString();
                        //dr["MRN"] = Convert.ToInt32(reader.GetValue(1).ToString());
                        dr["MRN"] = reader.GetValue(1).ToString();
                        //dr["DOS"] = Convert.ToDateTime(reader.GetValue(2).ToString());
                        dr["DOS"] = reader.GetValue(2).ToString();
                        dr["PhysicianName"] = reader.GetValue(3).ToString();
                        //dr["CaseID"] = Convert.ToInt32(reader.GetValue(4).ToString());
                        dr["CaseID"] = reader.GetValue(4).ToString();
                        dr["CaseStatus"] = reader.GetValue(5).ToString();
                        dr["FacilityName"] = reader.GetValue(6).ToString();
                        dr["CPTCode"] = reader.GetValue(7).ToString();
                        dr["CPTDesc"] = reader.GetValue(8).ToString();
                        if (reader.GetValue(9).ToString() == "")
                            dr["ScheduleOrder"] = 0;
                        else
                            //dr["ScheduleOrder"] = Convert.ToInt32(reader.GetValue(9).ToString());
                            dr["ScheduleOrder"] = reader.GetValue(9).ToString();
                        dr["PayorName"] = reader.GetValue(10).ToString();
                        dr["GroupNumber"] = reader.GetValue(11);
                        dr["InsuranceID"] = reader.GetValue(12).ToString();
                        dr["FileKey"] = FileKey;
                        dr["UploadDateTime"] = DateTime.Now;
                        dr["Username"] = "Scheduler";
                        dt.Rows.Add(dr);
                    }

                }
            }
            catch (Exception ex)
            {
                System.IO.File.Delete(filename);
                _logger.LogInformation("Worker running at: {time}", ex.Message);
                //string status = "Error";
                //string message = "Data in the file uploaded is in an invalid format.";

            }
            try
            {
                _logger.LogInformation("Worker before duming to importtable");
                using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(cs.Sqlconn))
                {
                    sqlBulkCopy.BulkCopyTimeout = 600;
                    sqlBulkCopy.DestinationTableName = "dbo.ImportTable";
                    cs.Sqlconn.Open();
                    sqlBulkCopy.WriteToServer(dt);
                }
                _logger.LogInformation("Worker before usp_ProcessImport");
                string ReturnString = helper.GetScalar("EXEC usp_ProcessImport '" + FileKey.ToString() + "'");
                _logger.LogInformation("Worker after usp_ProcessImport");
                //DataTable dt = helper.GetEmailList("ImportPatients");
                //SendMail MailManager = new SendMail();
                //MailManager.SendEmail(dt.Rows[0][2].ToString(), dt.Rows[0][3].ToString(), dt.Rows[0][4].ToString(), "File Import " + FileKey.ToString(), "File Import: " + FileKey.ToString() + " - " + "<br/>" + ReturnString);
                                                
            }
            catch (Exception ex)
            {
                System.IO.File.Delete(filename);
                _logger.LogInformation("Worker running at: {time}", ex.Message);

            }
            finally
            {
                cs.Sqlconn.Close();
            }
        }
        

    }
}