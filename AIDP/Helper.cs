﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace AIDP
{
    internal class Helper
    {
        Connection cs = new Connection();
        private readonly ILogger<Helper> _logger;
        public Helper(ILogger<Helper> logger)
        {
            _logger = logger;
        }
        public Helper()
        {
        }
        protected DataTable GetDatatable(string SQLQuery)
        {
            DataTable dt = new DataTable();
            CommandType cmdType = CommandType.Text;
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandType = cmdType;
                cmd.Connection = cs.Sqlconn;
                cmd.CommandText = SQLQuery;
                cmd.CommandTimeout = 0;

                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {

                    try
                    {
                        da.Fill(dt);
                    }
                    catch (Exception ex)
                    {
                        string err = ex.Message;
                    }
                    finally
                    {
                        da.Dispose();
                    }
                    return dt;
                }
            }
        }

        public DataTable GetEmailList(string emailEvent)
        {
            try
            {
                return GetDatatable("SELECT * from EmailConfig where emailEvent='" + emailEvent + "'");
            }
            catch (Exception ex)
            {
                _logger.LogInformation("No rows found in db." + ex.Message);
                return new DataTable();
            }
            
        }
        public DataTable getConfigPath()
        {            
            try
            {
                return GetDatatable("select * from configs where ConfigKey = 'ImportPatientPath'");
            }
            catch (Exception ex)
            {
                _logger.LogInformation("No rows found in db." + ex.Message);
                return new DataTable();
            }
        }

        public string GetScalar(string sqlQuery)
        {
            //var ReturnValue = null;
            string ReturnValueString;
            CommandType cmdType = CommandType.Text;

            SqlCommand cmd = new SqlCommand();
            cmd.CommandTimeout = 0;
            cmd.CommandType = cmdType;
            cmd.Connection = cs.Sqlconn;
            cmd.CommandText = sqlQuery;

            try
            {
                if (cmd.Connection.State == ConnectionState.Closed)
                    cmd.Connection.Open();
                var ReturnValue = cmd.ExecuteScalar();
                if (ReturnValue == DBNull.Value || ReturnValue == null)
                    ReturnValueString = "";
                else
                    ReturnValueString = ReturnValue.ToString();
            }
            catch (Exception ex)
            {
                ReturnValueString = "";
            }
            finally
            {
                cmd.Connection.Close();
            }

            return ReturnValueString.Trim();
        }
    }
}
