﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIDP
{
    internal class Connection
    {
        public SqlConnection Sqlconn;
        public SqlCommand SqlCmd = new SqlCommand();
        public Connection()
        {
            var configuration = GetConfiguration();
            Sqlconn = new SqlConnection(configuration.GetSection("ConnectionStrings").GetSection("IdentityConnection").Value);
        }
        public IConfigurationRoot GetConfiguration()
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            return builder.Build();
        }
    }
}
